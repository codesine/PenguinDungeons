package com.minecraftonline.penguindungeons.ai;

import net.minecraft.entity.EntityAreaEffectCloud;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityShulker;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;

import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.HasProjectileData;
import com.minecraftonline.penguindungeons.customentity.wither.LargeFireball;
import com.minecraftonline.penguindungeons.customentity.wither.SkullProjectile;
import com.minecraftonline.penguindungeons.customentity.wither.SmallFireball;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class AIUtil {

    public static void swapShulkerAttackForCustom(Shulker shulker, ShulkerBulletCreator shulkerBulletCreator, AITaskType aiTaskType, String name) {
        EntityShulker mcShulker = (EntityShulker) shulker;
        EntityShulker.AIAttack delegate = mcShulker.new AIAttack() {
            @Override
            public void updateTask() {
                AtomicInteger atomicInteger = new AtomicInteger(this.attackTime);
                AIUtil.shulkerAttackWithCustomBullet(mcShulker, atomicInteger, shulkerBulletCreator, name);
                this.attackTime = atomicInteger.get();
            }
        };

        Goal<Agent> normalGoals = shulker.getGoal(GoalTypes.NORMAL).get();
        AIUtil.removeShulkerAttack(normalGoals);


        normalGoals.addTask(4, new DelegatingToMCAI<Shulker>(aiTaskType, delegate));
    }

    public static void removeShulkerAttack(Goal<Agent> goal) {
        goal.getTasks().stream().filter(aiTask -> aiTask instanceof EntityShulker.AIAttack)
                .findAny().ifPresent(task -> goal.removeTask((AITask<? extends Agent>) task));
    }

    public interface ShulkerBulletCreator {
        EntityShulkerBullet create(World world, EntityLivingBase owner, EntityLivingBase target, EnumFacing.Axis axis);

        static ShulkerBulletCreator forCustom(Consumer<EntityLivingBase> onHit) {
            return (world, owner, target, axis) -> new ShulkerBulletCollideEffect(world, owner, target, axis, onHit);
        }

        static ShulkerBulletCreator forCustom(Consumer<EntityLivingBase> onHit, SoundEvent sound) {
            return (world, owner, target, axis) -> new ShulkerBulletCollideEffect(world, owner, target, axis, onHit, sound);
        }

        static ShulkerBulletCreator forCustomEffect(PotionEffect potionEffect) {
            return forCustom(forHit(potionEffect));
        }

        static ShulkerBulletCreator forClearEffects() {
            return forCustom(clearEffects());
        }

        static ShulkerBulletCreator forInvulnerable() {
            return (world, owner, target, axis) -> new ShulkerBulletInvulnerable(world, owner, target, axis);
        }

        static Consumer<EntityLivingBase> forHit(PotionEffect potionEffect) {
            return forHit(Arrays.asList(potionEffect));
        }

        static Consumer<EntityLivingBase> forHit2(List<net.minecraft.potion.PotionEffect> potionEffects) {
            return entity -> {
                Entity spongeEntity = (Entity)entity;
                List<PotionEffect> effects = spongeEntity.get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
                potionEffects.forEach((potionEffect) -> {
                    effects.add((PotionEffect) potionEffect);
                });
                spongeEntity.offer(Keys.POTION_EFFECTS, effects);
            };
        }

        static Consumer<EntityLivingBase> forHit(List<PotionEffect> potionEffects) {
            return entity -> {
                Entity spongeEntity = (Entity)entity;
                List<PotionEffect> effects = spongeEntity.get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
                potionEffects.forEach((potionEffect) -> {
                    effects.add(potionEffect);
                });
                spongeEntity.offer(Keys.POTION_EFFECTS, effects);
            };
        }

        static Consumer<EntityLivingBase> clearEffects() {
            return entity -> {
                Entity spongeEntity = (Entity)entity;
                spongeEntity.remove(Keys.POTION_EFFECTS);
            };
        }
    }

    public interface DirectShulkerBulletCreator {
        EntityShulkerBullet create(World world, EntityLivingBase shooter, EntityLivingBase target, double accelX, double accelY, double accelZ);

        static DirectShulkerBulletCreator forDirectBullet(Consumer<EntityLivingBase> onHit)
        {
            return (world, shooter, target, accelX, accelY, accelZ) -> new ShulkerBulletDirect(world, shooter, target, onHit, accelX, accelY, accelZ);
        }

        static DirectShulkerBulletCreator forDirectBullet(Consumer<EntityLivingBase> onHit, SoundEvent sound)
        {
            return (world, shooter, target, accelX, accelY, accelZ) -> new ShulkerBulletDirect(world, shooter, target, onHit, accelX, accelY, accelZ, sound);
        }
    }

    public interface ProjectileForImpact {
        // returns true if impact is valid and projectile should be set to dead
        boolean forImpact(RayTraceResult result, EntityFireball projectile);

        static ProjectileForImpact forMultipleImpacts(ProjectileForImpact... others) {
            return (result, projectile) -> {
                boolean hit = false;
                for (ProjectileForImpact base : others)
                {
                    boolean isHit = base.forImpact(result, projectile);
                    if (!hit && isHit) hit = isHit;
                }
                return hit;
            };
        }

        static ProjectileForImpact forImpactWithSound(ProjectileForImpact base, SoundEvent sound) {
            return (result, projectile) -> {
                boolean hit = base.forImpact(result, projectile);
                if (hit) projectile.playSound(sound, 1.0F, 0);
                return hit;
            };
        }

        static ProjectileForImpact forImpactDamage(float damage) {
            return forImpactDamage(damage, false);
        }

        static ProjectileForImpact forImpactDamage(float damage, boolean fireball) {
            return (result, projectile) -> {
                if (result.entityHit == null || !result.entityHit.isEntityEqual(projectile.shootingEntity)) {
                    if (result.entityHit != null) {
                        if (projectile.shootingEntity != null) {
                            if (fireball) result.entityHit.attackEntityFrom(DamageSource.causeFireballDamage(projectile, projectile.shootingEntity), damage);
                            else result.entityHit.attackEntityFrom(DamageSource.causeIndirectDamage(projectile, projectile.shootingEntity), damage);
                        } else {
                            result.entityHit.attackEntityFrom(DamageSource.MAGIC, damage);
                        }
                    }
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactAreaOfEffect(float radius, int duration, HasProjectileData data) {
            return (result, projectile) -> {
                if (result.entityHit == null || !result.entityHit.isEntityEqual(projectile.shootingEntity)) {
                    List<EntityLivingBase> list = projectile.world.<EntityLivingBase>getEntitiesWithinAABB(EntityLivingBase.class, projectile.getEntityBoundingBox().grow(4.0D, 2.0D, 4.0D));
                    EntityAreaEffectCloud entityareaeffectcloud = new EntityAreaEffectCloud(projectile.world, projectile.posX, projectile.posY, projectile.posZ);
                    entityareaeffectcloud.setOwner(projectile.shootingEntity);
                    entityareaeffectcloud.setParticle(data.getParticle());
                    entityareaeffectcloud.setParticleParam1(data.getParticleParam1());
                    entityareaeffectcloud.setParticleParam2(data.getParticleParam2());
                    entityareaeffectcloud.setRadius(radius);
                    entityareaeffectcloud.setDuration(duration);
                    entityareaeffectcloud.setRadiusPerTick((7.0F - entityareaeffectcloud.getRadius()) / (float)entityareaeffectcloud.getDuration());
                    data.getEffects().forEach((effect) -> {
                        entityareaeffectcloud.addEffect(effect);
                    });
                    if (!list.isEmpty()) {
                        for(EntityLivingBase entitylivingbase : list) {
                            double d0 = projectile.getDistanceSq(entitylivingbase);
                            if (d0 < 16.0D) {
                                entityareaeffectcloud.setPosition(entitylivingbase.posX, entitylivingbase.posY, entitylivingbase.posZ);
                                break;
                            }
                        }
                    }

                    projectile.world.spawnEntity(entityareaeffectcloud);
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactExplosion(float strength, boolean causesFire, boolean damageTerrain) {
            return (result, projectile) -> {
                if (result.entityHit == null || !result.entityHit.isEntityEqual(projectile.shootingEntity)) {
                    projectile.world.newExplosion(projectile, projectile.posX, projectile.posY, projectile.posZ, strength, causesFire, damageTerrain);
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactSetEffects(HasEffects data) {
            return (result, projectile) -> {
                if (result.entityHit == null || !result.entityHit.isEntityEqual(projectile.shootingEntity)) {
                    if (result.entityHit instanceof EntityLivingBase) {
                        data.getEffects().forEach((effect) -> {
                            ((EntityLivingBase)result.entityHit).addPotionEffect(effect);
                        });
                    }
                    projectile.world.newExplosion(projectile, projectile.posX, projectile.posY, projectile.posZ, 1.0F, false, false);
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactSetOnFire(int duration, float damage) {
            return (result, projectile) -> {
                if (result.entityHit == null || !result.entityHit.isEntityEqual(projectile.shootingEntity)) {
                    if (!result.entityHit.isImmuneToFire()) {
                        boolean flag = result.entityHit.attackEntityFrom(DamageSource.causeFireballDamage(projectile, projectile.shootingEntity), damage);
                        if (flag) {
                            result.entityHit.setFire(duration);
                        }
                    }
                    if (result.entityHit instanceof EntityLivingBase) {
                        ((EntityLivingBase)result.entityHit).setFire(duration);
                    }
                    return true;
                }
                return false;
            };
        }
    }

    public interface FireballCreator {
        EntityFireball create(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ);

        static FireballCreator forSkull(ProjectileForImpact forImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new SkullProjectile(world, shooter, accelX, accelY, accelZ, forImpact);
        }

        static FireballCreator forSkulls(ProjectileForImpact forImpact, ProjectileForImpact forBoostedImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new SkullProjectile(world, shooter, accelX, accelY, accelZ, forImpact, forBoostedImpact);
        }

        static FireballCreator forSkulls(ProjectileForImpact forImpact, ProjectileForImpact forBoostedImpact, float boostedChance)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new SkullProjectile(world, shooter, accelX, accelY, accelZ, forImpact, forBoostedImpact, boostedChance);
        }

        static FireballCreator forLargeFireball(ProjectileForImpact forImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new LargeFireball(world, shooter, accelX, accelY, accelZ, forImpact);
        }

        static FireballCreator forSmallFireball(ProjectileForImpact forImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new SmallFireball(world, shooter, accelX, accelY, accelZ, forImpact);
        }
    }

    public static void shulkerAttackWithCustomBullet(EntityShulker mcShulker, AtomicInteger attackTime, ShulkerBulletCreator shulkerBulletCreator, String name) {
        if (mcShulker.world.getDifficulty() != EnumDifficulty.PEACEFUL) {
            int attackTimeCur = attackTime.decrementAndGet();
            EntityLivingBase entitylivingbase = mcShulker.getAttackTarget();
            mcShulker.getLookHelper().setLookPositionWithEntity(entitylivingbase, 180.0F, 180.0F);
            double d0 = mcShulker.getDistanceSq(entitylivingbase);
            if (d0 < 400.0D) {
                if (attackTimeCur <= 0) {
                    attackTime.set(20 + mcShulker.world.rand.nextInt(10) * 20 / 2);
                    EntityShulkerBullet entityshulkerbullet = shulkerBulletCreator.create(mcShulker.world, mcShulker, entitylivingbase, mcShulker.getAttachmentFacing().getAxis());
                    entityshulkerbullet.setCustomNameTag(name);
                    mcShulker.world.spawnEntity(entityshulkerbullet);
                    mcShulker.playSound(SoundEvents.ENTITY_SHULKER_SHOOT, 2.0F, (mcShulker.world.rand.nextFloat() - mcShulker.world.rand.nextFloat()) * 0.2F + 1.0F);
                }
            } else {
                mcShulker.setAttackTarget(null);
            }
        }
    }
}
