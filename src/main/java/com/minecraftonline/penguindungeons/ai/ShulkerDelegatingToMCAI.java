package com.minecraftonline.penguindungeons.ai;

import net.minecraft.entity.ai.EntityAIBase;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.living.golem.Shulker;

public class ShulkerDelegatingToMCAI extends DelegatingToMCAI<Shulker> {
    public ShulkerDelegatingToMCAI(AITaskType type, EntityAIBase mcAITask) {
        super(type, mcAITask);
    }
}
