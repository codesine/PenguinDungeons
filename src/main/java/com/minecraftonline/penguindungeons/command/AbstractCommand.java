package com.minecraftonline.penguindungeons.command;

import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.util.Tuple;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractCommand implements Command {

    @Nullable
    private final String permission;
    private final List<Tuple<Command, List<String>>> children = new ArrayList<>();
    private final List<CommandElement> arguments = new ArrayList<>();
    private CommandExecutor executor = null;

    public AbstractCommand() {
        this(null);
    }

    public AbstractCommand(String permission) {
        this.permission = permission;
    }

    protected void setExecutor(CommandExecutor executor) {
        this.executor = executor;
    }

    protected void addArguments(CommandElement... argument) {
        this.arguments.addAll(Arrays.asList(argument));
    }

    protected void addChild(Command command, String primaryAlias, String... otherAliases) {
        List<String> aliases = new ArrayList<>(otherAliases.length + 1);
        aliases.add(primaryAlias);
        aliases.addAll(Arrays.asList(otherAliases));
        this.children.add(Tuple.of(command, aliases));
    }

    @Override
    public CommandSpec create() {
        CommandSpec.Builder builder = CommandSpec.builder();
        if (this.permission != null) {
            builder.permission(this.permission);
        }
        for (Tuple<Command, List<String>> childCommand : this.children) {
            builder.child(childCommand.getFirst().create(), childCommand.getSecond());
        }
        builder.arguments(this.arguments.toArray(new CommandElement[0]));
        if (this.executor != null) {
            builder.executor(this.executor);
        }
        return builder.build();
    }
}
