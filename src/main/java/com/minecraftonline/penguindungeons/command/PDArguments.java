package com.minecraftonline.penguindungeons.command;

import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.text.Text;

public class PDArguments {

    public static CommandElement dungeon(Text key) {
        return new DungeonCommandElement(key);
    }

}
