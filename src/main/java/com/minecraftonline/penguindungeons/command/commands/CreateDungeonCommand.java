package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.storage.WorldProperties;

public class CreateDungeonCommand extends AbstractCommand {

    public CreateDungeonCommand() {
        super(DungeonCommand.BASE_PERMISSION + "create");
        addArguments(GenericArguments.string(Text.of("name")), GenericArguments.world(Text.of("world")));
        setExecutor((src, args) -> {
            String name = args.requireOne("name");
            WorldProperties worldProperties = args.requireOne("world");
            try {
                Dungeon.checkNameValid(name);
            }
            catch(IllegalArgumentException e) {
                throw new CommandException(Text.of(e.getMessage()));
            }
            if (PenguinDungeons.getDungeons().containsKey(name)) {
                throw new CommandException(Text.of("Cannot create dungeon with name '" + name + "' as it already exists."));
            }
            Dungeon dungeon = new Dungeon(name, worldProperties.getUniqueId());
            PenguinDungeons.getDungeons().put(dungeon.name(), dungeon);
            src.sendMessage(Text.of(TextColors.GREEN, "Added dungeon " + dungeon.name()));
            return CommandResult.success();
        });
    }
}
