package com.minecraftonline.penguindungeons.config;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.config.serializer.BaseTriggerSerializer;
import com.minecraftonline.penguindungeons.config.serializer.DungeonSerializer;
import com.minecraftonline.penguindungeons.config.serializer.NBTEntityTypeSerializer;
import com.minecraftonline.penguindungeons.config.serializer.PDEntityTypeSerializer;
import com.minecraftonline.penguindungeons.config.serializer.UUIDSerializer;
import com.minecraftonline.penguindungeons.config.serializer.WeightedSpawnOptionSerializer;
import com.minecraftonline.penguindungeons.customentity.NBTEntityType;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import com.minecraftonline.penguindungeons.trigger.Trigger;
import com.minecraftonline.penguindungeons.util.PDTypeTokens;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.HeaderMode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializerCollection;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.util.TypeTokens;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConfigManager {

    private final Set<String> deletedDungeons = new HashSet<>();
    private final Path configDir;

    public ConfigManager(Path configDir) {
        this.configDir = configDir;
    }

    public void makeDirs() {
        try {
            Files.createDirectories(configDir);
        } catch (IOException e) {
            PenguinDungeons.getLogger().error("Failed to make directories for config", e);
        }
    }

    public void notifyDungeonDeleted(Dungeon dungeon) {
        this.deletedDungeons.add(dungeon.name());
    }

    public Collection<Dungeon> readDungeonsConfig() {
        try {
            CommentedConfigurationNode node = makeDungeonConfigLoader().load();
            List<Dungeon> dungeons = new ArrayList<>();
            for (Map.Entry<Object, ? extends ConfigurationNode> entry : node.getChildrenMap().entrySet()) {
                Dungeon dungeon = entry.getValue().getValue(PDTypeTokens.DUNGEON_TYPE_TOKEN);
                if (dungeon == null) {
                    continue;
                }
                dungeons.add(dungeon);
            }
            return dungeons;
        } catch (IOException | ObjectMappingException e) {
            PenguinDungeons.getLogger().error("Failed to load dungeons config", e);
            return Collections.emptyList();
        }
    }

    public Collection<Tuple<String, Trigger>> readTriggersConfig() {
        try {
            List<Tuple<String, Trigger>> triggers = new ArrayList<>();

            HoconConfigurationLoader loader = makeTriggerConfigLoader();
            CommentedConfigurationNode root = loader.load();
            for (Map.Entry<Object, ? extends ConfigurationNode> entry : root.getChildrenMap().entrySet()) {
                Trigger trigger = entry.getValue().getValue(PDTypeTokens.TRIGGER_TOKEN);
                if (trigger == null) {
                    continue;
                }
                triggers.add(Tuple.of(entry.getKey().toString(), trigger));
            }
            loader.save(root);
            return triggers;
        } catch (IOException | ObjectMappingException e) {
            PenguinDungeons.getLogger().error("Failed to load triggers config", e);
            return Collections.emptyList();
        }
    }

    public Collection<NBTEntityType> readEntitiesConfig() {
        try {
            List<NBTEntityType> entities = new ArrayList<>();
            HoconConfigurationLoader loader = makeEntitiesConfigLoader();
            CommentedConfigurationNode root = loader.load();
            for (Map.Entry<Object, ? extends ConfigurationNode> entry : root.getChildrenMap().entrySet()) {
                final NBTEntityType nbtEntityType = entry.getValue().getValue(PDTypeTokens.NBT_ENTITY_TYPE_TOKEN);
                if (nbtEntityType == null) {
                    continue;
                }
                entities.add(nbtEntityType);
            }
            return entities;
        } catch (IOException | ObjectMappingException e) {
            PenguinDungeons.getLogger().error("Failed to load entities config", e);
            return Collections.emptyList();
        }
    }

    @SuppressWarnings("UnstableApiUsage")
    public HoconConfigurationLoader makeDungeonConfigLoader() {
        TypeSerializerCollection typeSerializerCollection = TypeSerializerCollection.defaults()
                .register(PDTypeTokens.PD_ENTITY_TYPE_TOKEN, new PDEntityTypeSerializer())
                .register(PDTypeTokens.DUNGEON_TYPE_TOKEN, new DungeonSerializer())
                .register(PDTypeTokens.WEIGHTED_SPAWN_OPTION_TOKEN, new WeightedSpawnOptionSerializer())
                .register(TypeTokens.UUID_TOKEN, new UUIDSerializer());
        ConfigurationOptions configurationOptions = ConfigurationOptions.defaults().withSerializers(typeSerializerCollection);
        return HoconConfigurationLoader.builder()
                .setPath(configDir.resolve("dungeons.conf"))
                .setDefaultOptions(configurationOptions)
                .build();
    }

    private static final String TRIGGER_HEADER = "This file allows you to configure triggers that spawn dungeons.\n" +
            "These should be added in the format similar to the following:\n" +
            "your_custom_name: {\n" +
            "  \"trigger_type\"=\"summon\"\n" +
            "  entity=\"ender_dragon\"\n" +
            "  dungeons=[\n" +
            "    \"dragonfight\"\n" +
            "  ]\n" +
            "}";

    @SuppressWarnings("UnstableApiUsage")
    public HoconConfigurationLoader makeTriggerConfigLoader() {
        TypeSerializerCollection typeSerializerCollection = TypeSerializerCollection.defaults()
                .register(PDTypeTokens.TRIGGER_TOKEN, new BaseTriggerSerializer());
        ConfigurationOptions configurationOptions = ConfigurationOptions.defaults()
                .withSerializers(typeSerializerCollection)
                .setHeader(TRIGGER_HEADER);
        return HoconConfigurationLoader.builder()
                .setPath(configDir.resolve("triggers.conf"))
                .setDefaultOptions(configurationOptions)
                .setHeaderMode(HeaderMode.PRESET)
                .build();
    }

    public HoconConfigurationLoader makeEntitiesConfigLoader() {
        TypeSerializerCollection typeSerializerCollection = TypeSerializerCollection.defaults()
                .register(PDTypeTokens.NBT_ENTITY_TYPE_TOKEN, new NBTEntityTypeSerializer());
        ConfigurationOptions configurationOptions = ConfigurationOptions.defaults()
                .withSerializers(typeSerializerCollection);
        return HoconConfigurationLoader.builder()
                .setPath(configDir.resolve("entities.conf"))
                .setDefaultOptions(configurationOptions)
                .build();
    }

    public boolean saveEntities(Collection<NBTEntityType> nbtEntityTypes) {
        try {
            PenguinDungeons.getLogger().info("Saving custom entities: " + nbtEntityTypes);
            HoconConfigurationLoader loader = makeEntitiesConfigLoader();
            CommentedConfigurationNode root = loader.load();
            for (NBTEntityType nbtEntityType : nbtEntityTypes) {
                if (!nbtEntityType.getId().namespace().equals(ResourceKey.PENGUIN_DUNGEONS_CONFIG_NAMESPACE)) {
                    PenguinDungeons.getLogger().error("An NBTEntityType had a key of '" + nbtEntityType.getId() + "', expected namespace: '" + ResourceKey.PENGUIN_DUNGEONS_CONFIG_NAMESPACE + "' for something being serialized to config.");
                    continue;
                }
                root.getNode(nbtEntityType.getId().value()).setValue(PDTypeTokens.NBT_ENTITY_TYPE_TOKEN, nbtEntityType);
            }
            loader.save(root);
            return true;
        } catch (IOException | ObjectMappingException e) {
            PenguinDungeons.getLogger().error("Failed to save entities config", e);
            return false;
        }
    }

    public boolean saveDungeons(final Collection<Dungeon> dungeons) {
        try {
            HoconConfigurationLoader loader = makeDungeonConfigLoader();
            CommentedConfigurationNode root = loader.load();
            for (Map.Entry<Object, ? extends ConfigurationNode> entry : root.getChildrenMap().entrySet()) {
                if (this.deletedDungeons.contains(entry.getValue().getNode("name").getString())) {
                    entry.getValue().setValue(null); // Yeet.
                }
            }
            this.deletedDungeons.clear();

            for (Dungeon dungeon : dungeons) {
                root.getNode(dungeon.name()).setValue(PDTypeTokens.DUNGEON_TYPE_TOKEN, dungeon);
            }
            loader.save(root);
            return true;
        } catch (IOException | ObjectMappingException e) {
            PenguinDungeons.getLogger().error("Failed to save dungeons config", e);
            return false;
        }
    }
}
