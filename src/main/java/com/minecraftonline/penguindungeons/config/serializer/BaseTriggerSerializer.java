package com.minecraftonline.penguindungeons.config.serializer;

import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.trigger.Trigger;
import com.minecraftonline.penguindungeons.trigger.TriggerType;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

@SuppressWarnings("UnstableApiUsage")
public class BaseTriggerSerializer implements TypeSerializer<Trigger> {

    private static final String TRIGGER_TYPE_NODE = "trigger_type";

    @Override
    public @Nullable Trigger deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
        ConfigurationNode node = value.getNode(TRIGGER_TYPE_NODE);
        String triggerTypeStr = node.getString();
        if (triggerTypeStr == null) {
            throw new ObjectMappingException(TRIGGER_TYPE_NODE + " cannot be null");
        }
        TriggerType triggerType;
        try {
            triggerType = TriggerType.valueOf(triggerTypeStr.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new ObjectMappingException("Invalid " + TRIGGER_TYPE_NODE + " value");
        }

        return value.getValue(triggerType.typeToken());
    }

    @Override
    public void serialize(@NonNull TypeToken<?> type, @Nullable Trigger obj, @NonNull ConfigurationNode value) throws ObjectMappingException {
        if (obj == null) {
            value.setValue(null);
            return;
        }
        value.getNode(TRIGGER_TYPE_NODE).setValue(obj.getType().name().toLowerCase());
        set(value, obj, obj.getType().typeToken());
    }

    @SuppressWarnings("unchecked")
    private static <T extends Trigger> void set(ConfigurationNode node, Trigger trigger, TypeToken<T> typeToken) throws ObjectMappingException {
        node.setValue(typeToken, (T) trigger);
    }
}
