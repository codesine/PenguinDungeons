package com.minecraftonline.penguindungeons.customentity;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;

import java.util.List;

public interface CustomEntityType extends PDEntityType {

    static final int TICKS_PER_SECOND = 20;

    /**
     * Loads anything required on load.
     * Any custom AI must be re-applied here,
     * or the entity will lose its custom ai.
     * @param entity Entity
     */
    default void onLoad(Entity entity) {}

    List<Text> getDescription();
}
