package com.minecraftonline.penguindungeons.customentity;

import com.google.common.collect.ImmutableSet;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.bee.AngryBee;
import com.minecraftonline.penguindungeons.customentity.bee.AngryEnbee;
import com.minecraftonline.penguindungeons.customentity.bee.ZombieBee;
import com.minecraftonline.penguindungeons.customentity.dragon.RobotDragon;
import com.minecraftonline.penguindungeons.customentity.dragon.TerrainDragon;
import com.minecraftonline.penguindungeons.customentity.golem.ExplodingSnowGolem;
import com.minecraftonline.penguindungeons.customentity.golem.FrostGolem;
import com.minecraftonline.penguindungeons.customentity.golem.IceSnowGolem;
import com.minecraftonline.penguindungeons.customentity.golem.SnowGolemType;
import com.minecraftonline.penguindungeons.customentity.golem.SnowflakeGolem;
import com.minecraftonline.penguindungeons.customentity.shulker.ChaosShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.ShulkerType;
import com.minecraftonline.penguindungeons.customentity.shulker.potion.ClearEffectShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.potion.GlowingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.potion.PotionEffectShulkers;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.DragonShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.ExperienceShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.ExplodingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.FieryShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.HealthyShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.InvisibleShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.NormalShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.PaladinShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.RainbowShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.RegeneratingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.SniperShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.SpikeyShulker;
import com.minecraftonline.penguindungeons.customentity.wither.PumpkinWither;
import com.minecraftonline.penguindungeons.customentity.zombie.FireworkZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.GiantZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.HorseZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.LaserZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.NinjaZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.NormalZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.PoliceZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.PriestZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.PumpkinZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.ShieldZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.ZombieDog;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.value.ValueContainer;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@SuppressWarnings("unused")
public class CustomEntityTypes {

    private static Map<ResourceKey, CustomEntityType> REGISTRY = null;

    public static final ShulkerType NORMAL_SHULKER         = new NormalShulker(ResourceKey.pd("shulker_normal"));
    public static final ShulkerType HEALTHY_SHULKER        = new HealthyShulker(ResourceKey.pd("shulker_healthy"));
    public static final ShulkerType GLOWING_SHULKER        = new GlowingShulker(ResourceKey.pd("shulker_glowing"));
    public static final ShulkerType INVISIBLE_SHULKER      = new InvisibleShulker(ResourceKey.pd("shulker_invisible"));
    public static final ShulkerType REGENERATING_SHULKER   = new RegeneratingShulker(ResourceKey.pd("shulker_regenerating"));
    public static final ShulkerType SNIPER_SHULKER         = new SniperShulker(ResourceKey.pd("shulker_sniper"));
    public static final ShulkerType EXPLODING_SHULKER      = new ExplodingShulker(ResourceKey.pd("shulker_exploding"));
    public static final ShulkerType FIERY_SHULKER          = new FieryShulker(ResourceKey.pd("shulker_fiery"));
    public static final ShulkerType SPIKEY_SHULKER         = new SpikeyShulker(ResourceKey.pd("shulker_spikey"));
    public static final ShulkerType POISON_SHULKER         = PotionEffectShulkers.poison(ResourceKey.pd("shulker_poison"));
    public static final ShulkerType JUMP_BOOST_SHULKER     = PotionEffectShulkers.jumpBoost(ResourceKey.pd("shulker_jump_boost"));
    public static final ShulkerType MINING_FATIGUE_SHULKER = PotionEffectShulkers.miningFatigue(ResourceKey.pd("shulker_mining_fatigue"));
    public static final ShulkerType SLOWNESS_SHULKER       = PotionEffectShulkers.slowness(ResourceKey.pd("shulker_slowness"));
    public static final ShulkerType SPEED_SHULKER          = PotionEffectShulkers.speed(ResourceKey.pd("shulker_speed"));
    public static final ShulkerType WITHER_SHULKER         = PotionEffectShulkers.wither(ResourceKey.pd("shulker_wither"));
    public static final ShulkerType BLIDNING_SHULKER       = PotionEffectShulkers.blindness(ResourceKey.pd("shulker_blinding"));
    public static final ShulkerType CLEAR_SHULKER          = new ClearEffectShulker(ResourceKey.pd("shulker_clear"));
    public static final ShulkerType EXPERIENCE_SHULKER     = new ExperienceShulker(ResourceKey.pd("shulker_experience"));
    public static final ShulkerType PALADIN_SHULKER        = new PaladinShulker(ResourceKey.pd("shulker_paladin"));
    public static final ShulkerType DRAGON_SHULKER         = new DragonShulker(ResourceKey.pd("shulker_dragon"));
    public static final ShulkerType RAINBOW_SHULKER        = new RainbowShulker(ResourceKey.pd("shulker_rainbow"));
    public static final ShulkerType CHAOS_SHULKER          = new ChaosShulker(ResourceKey.pd("shulker_chaos"));
    public static final SnowGolemType SNOWFLAKE_GOLEM      = new SnowflakeGolem(ResourceKey.pd("snow_golem_snowflake"));
    public static final SnowGolemType EXPLODING_SNOW_GOLEM = new ExplodingSnowGolem(ResourceKey.pd("snow_golem_explode"));
    public static final SnowGolemType ICE_SNOW_GOLEM       = new IceSnowGolem(ResourceKey.pd("snow_golem_ice"));
    public static final CustomPolarBear POLAR_BEAR         = new CustomPolarBear(ResourceKey.pd("polar_bear_custom"));
    public static final TerrainDragon TERRAIN_DRAGON       = new TerrainDragon(ResourceKey.pd("terrain_dragon"));
    public static final RobotDragon ROBOT_DRAGON           = new RobotDragon(ResourceKey.pd("robot_dragon"));
    public static final NinjaZombie NINJA_ZOMBIE           = new NinjaZombie(ResourceKey.pd("ninja_zombie"));
    public static final ZombieDog ZOMBIE_DOG               = new ZombieDog(ResourceKey.pd("zombie_dog"));
    public static final HorseZombie HORSE_ZOMBIE           = new HorseZombie(ResourceKey.pd("horse_zombie"));
    public static final LaserZombie LASER_ZOMBIE           = new LaserZombie(ResourceKey.pd("laser_zombie"));
    public static final GiantZombie GIANT_ZOMBIE           = new GiantZombie(ResourceKey.pd("giant_zombie"));
    public static final ShieldZombie SHIELD_ZOMBIE         = new ShieldZombie(ResourceKey.pd("shield_zombie"));
    public static final PumpkinZombie PUMPKIN_ZOMBIE       = new PumpkinZombie(ResourceKey.pd("pumpkin_zombie"));
    public static final PumpkinWither PUMPKIN_WITHER       = new PumpkinWither(ResourceKey.pd("pumpkin_wither"));
    public static final AngryBee ANGRY_BEE                 = new AngryBee(ResourceKey.pd("angry_bee"));
    public static final AngryEnbee ANGRY_ENBEE             = new AngryEnbee(ResourceKey.pd("angry_enbee"));
    public static final ZombieBee ZOMBIE_BEE               = new ZombieBee(ResourceKey.pd("zombie_bee"));
    public static final PoliceZombie POLICE_ZOMBIE         = new PoliceZombie(ResourceKey.pd("police_zombie"));
    public static final NormalZombie NORMAL_ZOMBIE         = new NormalZombie(ResourceKey.pd("normal_zombie"));
    public static final PriestZombie PRIEST_ZOMBIE         = new PriestZombie(ResourceKey.pd("priest_zombie"));
    public static final FireworkZombie FIREWORK_ZOMBIE     = new FireworkZombie(ResourceKey.pd("firework_zombie"));
    public static final FrostGolem FROST_GOLEM             = new FrostGolem(ResourceKey.pd("frost_golem"));

    public static void initRegistry() {
        if (REGISTRY != null) {
            throw new IllegalStateException("Registry is already initialised!");
        }
        REGISTRY = new HashMap<>();
        for (Field field : CustomEntityTypes.class.getFields()) {
            if (!CustomEntityType.class.isAssignableFrom(field.getType())) {
                return;
            }
            try {
                CustomEntityType entityType = (CustomEntityType) field.get(null);
                REGISTRY.put(entityType.getId(), entityType);
            } catch (IllegalAccessException e) {
                PenguinDungeons.getLogger().error("Error initialising registry", e);
            }
        }
    }

    public static Optional<CustomEntityType> getById(final ResourceKey key) {
        return Optional.of(REGISTRY.get(key));
    }

    public static Optional<CustomEntityType> get(ValueContainer<?> valueContainer) {
        return valueContainer.get(PenguinDungeonKeys.PD_ENTITY_TYPE).flatMap(CustomEntityTypes::getById);
    }

    public static Collection<CustomEntityType> getAll() {
        return ImmutableSet.copyOf(REGISTRY.values());
    }
}
