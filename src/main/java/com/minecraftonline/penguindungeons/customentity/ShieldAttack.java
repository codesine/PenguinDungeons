package com.minecraftonline.penguindungeons.customentity;

import org.spongepowered.api.entity.living.Creature;

import static com.minecraftonline.penguindungeons.customentity.CustomEntityType.TICKS_PER_SECOND;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumHand;

public class ShieldAttack extends EntityAIAttackMelee {

   private int shieldTick;
   private final EnumHand shieldHand;
   
   private int shieldUntil = TICKS_PER_SECOND*2; // stop shielding for 2 seconds
   private int shieldDuration = TICKS_PER_SECOND*12; // shield for 10 seconds at a time 

   public ShieldAttack(EntityCreature creatureIn, double speedIn, boolean longMemoryIn) {
       this(creatureIn, speedIn, longMemoryIn, EnumHand.MAIN_HAND);
   }

   public ShieldAttack(EntityCreature creatureIn, double speedIn, boolean longMemoryIn, EnumHand hand) {
      super(creatureIn, speedIn, longMemoryIn);
      this.shieldHand = hand;
      // start already shielding
      shield(true);
   }

   /**
    * Execute a one shot task or start executing a continuous task
    */
   public void startExecuting() {
      super.startExecuting();
      this.shieldTick = this.shieldDuration;
   }

   /**
    * Reset the task's internal state. Called when this task is interrupted by another one
    */
   public void resetTask() {
      super.resetTask();
      shield(true); // shield by default
   }

   private void shield(boolean shielding)
   {
       // only update if different
       boolean currentlyShielding = this.attacker.isHandActive() && (this.attacker.getActiveHand() == this.shieldHand);
       if (shielding == currentlyShielding) return;

       if (shielding)
       {
           this.attacker.setActiveHand(this.shieldHand);
       }
       else
       {
           this.attacker.stopActiveHand();
       }
       // should not take knockback while shielding
       this.attacker.getEntityAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE)
                    .setBaseValue(shielding ? 1.0D : 0.0D);
       // raise arms if possible when not shielding
       if (this.attacker instanceof EntityZombie)
       {
           EntityZombie zombie = (EntityZombie) this.attacker;
           zombie.setArmsRaised(!shielding);
       }
   }

   /**
    * Keep ticking a continuous task that has already been started
    */
   public void updateTask() {
      super.updateTask();
      --this.shieldTick;
      if (this.shieldTick <= 0) this.shieldTick = this.shieldDuration;
      if (this.shieldTick <= this.shieldUntil) {
          // shielded enough, stop shielding
          shield(false);
      } else {
          // shield again
          shield(true);
      }

   }

   @Override
   protected void checkAndPerformAttack(EntityLivingBase enemy, double distToEnemySqr) {
       double d0 = this.getAttackReachSqr(enemy);
       if (distToEnemySqr <= d0 && this.attackTick <= 0) {
          this.attackTick = 20;
          this.shieldTick = shieldUntil; // stop shielding when attacking
          if (shieldHand == EnumHand.MAIN_HAND)
          {
              this.attacker.playSound(SoundEvents.ITEM_SHIELD_BLOCK, 1.0F, 1.0F);
          }
          this.attacker.swingArm(EnumHand.MAIN_HAND);
          this.attacker.attackEntityAsMob(enemy);
       }

    }

   public static class AttackWithShield extends DelegatingToMCAI<Creature> {

       public AttackWithShield(Creature creature, double speedIn, boolean longMemoryIn) {
           this(creature, speedIn, longMemoryIn, EnumHand.MAIN_HAND);
       }

       public AttackWithShield(Creature creature, double speedIn, boolean longMemoryIn, EnumHand hand) {
           super(PenguinDungeonAITaskTypes.SHIELD_ATTACK,
                   new ShieldAttack((EntityCreature) creature, speedIn, longMemoryIn, hand));
       }
   }
}
