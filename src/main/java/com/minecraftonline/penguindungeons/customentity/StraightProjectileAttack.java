package com.minecraftonline.penguindungeons.customentity;

import java.util.Optional;

import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.living.Creature;

import static com.minecraftonline.penguindungeons.customentity.CustomEntityType.TICKS_PER_SECOND;

import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.ai.AIUtil.DirectShulkerBulletCreator;
import com.minecraftonline.penguindungeons.ai.AIUtil.FireballCreator;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.util.math.MathHelper;

public class StraightProjectileAttack<T extends EntityCreature> extends EntityAIBase {

    /** The entity the AI instance has been applied to */
    private final EntityLiving entityHost;
    /** The entity (as a RangedAttackMob) the AI instance has been applied to. */
    private final Creature rangedAttackEntityHost;
    private EntityLivingBase attackTarget;
    /**
     * A decrementing tick that spawns a ranged attack once this value reaches 0. It is then set back to the
     * maxRangedAttackTime.
     */
    private int rangedAttackTime;
    private final double entityMoveSpeed;
    private int seeTime;
    private final int attackIntervalMin;
    /** The maximum time the AI has to wait before performing another ranged attack. */
    private final int maxRangedAttackTime;
    private final float attackRadius;
    private final float maxAttackDistance;
    private Optional<String> projectileName = Optional.empty();
    private DirectShulkerBulletCreator bulletCreator = null;
    private FireballCreator fireballCreator = null;

    public StraightProjectileAttack(Creature attacker) {
        this(attacker, 1.25D, 25, 45, 15.0F);
    }

    public StraightProjectileAttack(Creature attacker, double movespeed, int maxAttackTime, float maxAttackDistanceIn) {
       this(attacker, movespeed, maxAttackTime, maxAttackTime, maxAttackDistanceIn);
    }

    public StraightProjectileAttack(Creature attacker, double movespeed, int p_i1650_4_, int maxAttackTime, float maxAttackDistanceIn) {
       this.rangedAttackTime = -1;
       if (!(attacker instanceof EntityLivingBase)) {
          throw new IllegalArgumentException("AttackGoal requires Mob implements EntityLivingBase");
       } else {
          this.rangedAttackEntityHost = attacker;
          this.entityHost = (EntityLiving)attacker;
          this.entityMoveSpeed = movespeed;
          this.attackIntervalMin = p_i1650_4_;
          this.maxRangedAttackTime = maxAttackTime;
          this.attackRadius = maxAttackDistanceIn;
          this.maxAttackDistance = maxAttackDistanceIn * maxAttackDistanceIn;
          this.setMutexBits(3);
       }
    }

    public StraightProjectileAttack<T> setFireballCreator(FireballCreator creator)
    {
        fireballCreator = creator;
        bulletCreator = null;
        return this;
    }

    public StraightProjectileAttack<T> setDirectShulkerBulletCreator(DirectShulkerBulletCreator creator)
    {
        fireballCreator = null;
        bulletCreator = creator;
        return this;
    }

    public StraightProjectileAttack<T> setName(Optional<String> name)
    {
        projectileName = name;
        return this;
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute() {
       EntityLivingBase entitylivingbase = this.entityHost.getAttackTarget();
       if (entitylivingbase == null || !entitylivingbase.isEntityAlive()) {
          return false;
       } else {
          this.attackTarget = entitylivingbase;
          return true;
       }
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean shouldContinueExecuting() {
       return this.shouldExecute() || !this.entityHost.getNavigator().noPath();
    }

    /**
     * Reset the task's internal state. Called when this task is interrupted by another one
     */
    public void resetTask() {
       this.attackTarget = null;
       this.seeTime = 0;
       this.rangedAttackTime = -1;
    }

    /**
     * Keep ticking a continuous task that has already been started
     */
    public void updateTask() {
       double d0 = this.entityHost.getDistanceSq(this.attackTarget.posX, this.attackTarget.getEntityBoundingBox().minY, this.attackTarget.posZ);
       boolean flag = this.entityHost.getEntitySenses().canSee(this.attackTarget);
       if (flag) {
          ++this.seeTime;
       } else {
          this.seeTime = 0;
       }

       if (d0 <= (double)this.maxAttackDistance && this.seeTime >= 20) {
          this.entityHost.getNavigator().clearPath();
       } else {
          this.entityHost.getNavigator().tryMoveToEntityLiving(this.attackTarget, this.entityMoveSpeed);
       }

       this.entityHost.getLookHelper().setLookPositionWithEntity(this.attackTarget, 30.0F, 30.0F);
       if (--this.rangedAttackTime == 0) {
          if (!flag) {
             return;
          }

          float f = MathHelper.sqrt(d0) / this.attackRadius;

          double d1 = this.attackTarget.posX - this.entityHost.posX;
          double d2 = this.attackTarget.getEntityBoundingBox().minY + (double)(this.attackTarget.height / 2.0F) - (this.entityHost.posY + (double)(this.entityHost.height / 2.0F));
          double d3 = this.attackTarget.posZ - this.entityHost.posZ;
          @SuppressWarnings("unchecked")
          T creature = (T) this.rangedAttackEntityHost;
          if (bulletCreator != null)
          {
              EntityShulkerBullet entityshulkerbullet = bulletCreator.create(creature.world, creature, this.attackTarget, d1, d2, d3);
              if (projectileName.isPresent()) entityshulkerbullet.setCustomNameTag(projectileName.get());
              creature.world.spawnEntity(entityshulkerbullet);
          }
          else if (fireballCreator != null) {
              EntityFireball fireball = fireballCreator.create(creature.world, creature, d1, d2, d3);
              if (projectileName.isPresent()) fireball.setCustomNameTag(projectileName.get());
              creature.world.spawnEntity(fireball);
          }
          this.rangedAttackTime = MathHelper.floor(f * (float)(this.maxRangedAttackTime - this.attackIntervalMin) + (float)this.attackIntervalMin);
       } else if (this.rangedAttackTime < 0) {
          float f2 = MathHelper.sqrt(d0) / this.attackRadius;
          this.rangedAttackTime = MathHelper.floor(f2 * (float)(this.maxRangedAttackTime - this.attackIntervalMin) + (float)this.attackIntervalMin);
       }

    }

    public static class RangedAttack extends DelegatingToMCAI<Creature> {

        public RangedAttack(Creature creature) {
            this(creature, Optional.empty());
        }

        public RangedAttack(Creature creature, Optional<String> name) {
            // default slowness with glass break sound
            this(creature, name, DirectShulkerBulletCreator.forDirectBullet(
                    AIUtil.ShulkerBulletCreator.forHit(PotionEffect.of(PotionEffectTypes.SLOWNESS, 1, TICKS_PER_SECOND*10))));
        }

        public RangedAttack(Creature creature, Optional<String> name, FireballCreator creator) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityCreature>(creature)
                    .setName(name)
                    .setFireballCreator(creator));
        }

        public RangedAttack(Creature creature, Optional<String> name, FireballCreator creator,
                double movespeed, int maxAttackTime, float maxAttackDistance) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityCreature>(creature, movespeed, maxAttackTime, maxAttackDistance)
                    .setName(name)
                    .setFireballCreator(creator));
        }

        public RangedAttack(Creature creature, Optional<String> name, FireballCreator creator,
                double movespeed, int minAttackTime, int maxAttackTime, float maxAttackDistance) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityCreature>(creature, movespeed, minAttackTime, maxAttackTime, maxAttackDistance)
                    .setName(name)
                    .setFireballCreator(creator));
        }

        public RangedAttack(Creature creature, Optional<String> name, DirectShulkerBulletCreator creator) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityCreature>(creature)
                    .setName(name)
                    .setDirectShulkerBulletCreator(creator));
        };

        public RangedAttack(Creature creature, Optional<String> name, DirectShulkerBulletCreator creator,
                double movespeed, int maxAttackTime, float maxAttackDistance) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityCreature>(creature, movespeed, maxAttackTime, maxAttackDistance)
                    .setName(name)
                    .setDirectShulkerBulletCreator(creator));
        }

        public RangedAttack(Creature creature, Optional<String> name, DirectShulkerBulletCreator creator,
                double movespeed, int minAttackTime, int maxAttackTime, float maxAttackDistance) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityCreature>(creature, movespeed, minAttackTime, maxAttackTime, maxAttackDistance)
                    .setName(name)
                    .setDirectShulkerBulletCreator(creator));
        }
    }
}