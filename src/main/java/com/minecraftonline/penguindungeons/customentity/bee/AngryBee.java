package com.minecraftonline.penguindungeons.customentity.bee;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.living.monster.Husk;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.util.ResourceKey;

public class AngryBee extends BeeType<Husk> {

    public AngryBee(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Angry Bee");
    }

    @Override
    public Text getEntityName() {
        return Text.of(TextColors.RESET, "Bee");
    }

    @Override
    protected String getTexture() {
        return "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmRlMWY1Nzc2NjY4M2E5MjQ2NGNlNWVmOGJkZDBmM2M5MjgyZmU3NmJiYWUxNzg5NDA2ZTBhMmRjYmRmODRhYiJ9fX0=";
    }

    @Override
    protected String getUUID() {
        return "72bbc2ef-8cb9-56df-ac3e-3989045d5c83";
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "An angry bee"));
    }

}
