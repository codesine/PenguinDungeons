package com.minecraftonline.penguindungeons.customentity.dragon;

import java.util.List;

import net.minecraft.entity.EntityAreaEffectCloud;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityDragonFireball;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;

public class CustomDragonFireball extends EntityDragonFireball {
    protected CustomDragon dragon;

    public CustomDragonFireball(CustomDragon dragon) {
        super(dragon.world);
        this.dragon = dragon;
    }

    public CustomDragonFireball(CustomDragon dragon, double accelX, double accelY, double accelZ) {
        super(dragon.world, dragon, accelX, accelY, accelZ);
        this.dragon = dragon;
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        if (result.entityHit == null || !result.entityHit.isEntityEqual(this.shootingEntity)) {
            if (!this.world.isRemote) {
                List<EntityLivingBase> list = this.world.<EntityLivingBase>getEntitiesWithinAABB(EntityLivingBase.class, this.getEntityBoundingBox().grow(4.0D, 2.0D, 4.0D));
                EntityAreaEffectCloud entityareaeffectcloud = new EntityAreaEffectCloud(this.world, this.posX, this.posY, this.posZ);
                entityareaeffectcloud.setOwner(this.shootingEntity);
                entityareaeffectcloud.setParticle(this.dragon.getParticle());
                entityareaeffectcloud.setParticleParam1(this.dragon.getParticleParam1());
                entityareaeffectcloud.setParticleParam2(this.dragon.getParticleParam2());
                entityareaeffectcloud.setRadius(3.0F);
                entityareaeffectcloud.setDuration(600);
                entityareaeffectcloud.setRadiusPerTick((7.0F - entityareaeffectcloud.getRadius()) / (float)entityareaeffectcloud.getDuration());
                this.dragon.getEffects().forEach((effect) -> {
                    entityareaeffectcloud.addEffect(effect);
                });
                if (!list.isEmpty()) {
                    for(EntityLivingBase entitylivingbase : list) {
                        double d0 = this.getDistanceSq(entitylivingbase);
                        if (d0 < 16.0D) {
                            entityareaeffectcloud.setPosition(entitylivingbase.posX, entitylivingbase.posY, entitylivingbase.posZ);
                            break;
                        }
                    }
                }

                this.world.playEvent(2006, new BlockPos(this.posX, this.posY, this.posZ), 0);
                this.world.spawnEntity(entityareaeffectcloud);
                this.setDead();
            }

        }
    }
}
