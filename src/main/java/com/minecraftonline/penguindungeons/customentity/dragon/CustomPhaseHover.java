package com.minecraftonline.penguindungeons.customentity.dragon;

import net.minecraft.entity.boss.dragon.phase.PhaseHover;

public class CustomPhaseHover extends PhaseHover implements CustomIPhase {

    public CustomPhaseHover(CustomDragon dragonIn) {
        super(dragonIn);
    }

    @Override
    public CustomPhaseList<? extends CustomIPhase> getCustomType() {
        return CustomPhaseList.HOVER;
    }

}
