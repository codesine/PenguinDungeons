package com.minecraftonline.penguindungeons.customentity.dragon;

import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.boss.dragon.phase.IPhase;
import net.minecraft.entity.boss.dragon.phase.PhaseManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CustomPhaseManager extends PhaseManager {
    private static final Logger LOGGER = LogManager.getLogger();
    private final CustomDragon dragon;
    private final IPhase[] phases = new IPhase[CustomPhaseList.getTotalPhases()];
    private CustomIPhase phase;

    public CustomPhaseManager(CustomDragon dragonIn) {
        super(dragonIn);
        this.dragon = dragonIn;
        this.setPhase(CustomPhaseList.TAKEOFF);
    }

    public void setPhase(CustomPhaseList<?> phaseIn) {
        if (this.phase == null || phaseIn != this.phase.getCustomType()) {
            if (this.phase != null) {
                this.phase.removeAreaEffect();
            }

            this.phase = this.getPhase(phaseIn);
            if (!this.dragon.world.isRemote) {
                this.dragon.getDataManager().set(EntityDragon.PHASE, Integer.valueOf(phaseIn.getId()));
            }

            LOGGER.debug("Dragon is now in phase {} on the {}", phaseIn, this.dragon.world.isRemote ? "client" : "server");
            this.phase.initPhase();
        }
    }

    public CustomIPhase getCurrentPhase() {
        return this.phase;
    }

    public <T extends CustomIPhase> T getPhase(CustomPhaseList<?> phaseIn) {
        int i = phaseIn.getId();
        if (this.phases[i] == null) {
            this.phases[i] = phaseIn.createPhase(this.dragon);
        }

        return (T)this.phases[i];
    }
}
