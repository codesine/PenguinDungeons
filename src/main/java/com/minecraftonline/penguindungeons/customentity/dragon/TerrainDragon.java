package com.minecraftonline.penguindungeons.customentity.dragon;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.complex.EnderDragon;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.block.Block;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BossInfo;
import net.minecraft.world.end.DragonFightManager;

public class TerrainDragon extends AbstractCustomEntity implements DragonType {

    public static final double ORIGINAL_DRAGON_HEALTH = 200;
    public static final double NEW_HEALTH = ORIGINAL_DRAGON_HEALTH * 3;

    public TerrainDragon(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.GOLD, "Terrain Dragon");
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ENDER_DRAGON;
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Terrain Dragon"))
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeDragon(world, pos, null), this::onLoad);
    }

    public EnderDragon makeDragon(World world, Vector3d blockPos, DragonFightManager fightManager) {
        PenguinDungeons.getLogger().debug("Making Terrain Dragon");
        EntityDragon dragon;
        BlockPos start = new BlockPos(blockPos.getX(), blockPos.getY(), blockPos.getZ());
        CustomDragon cDragon = new CustomDragon((net.minecraft.world.World) world, BossInfo.Color.YELLOW, fightManager);
        cDragon.setPodium(start);
        cDragon.setEffects(Arrays.asList(
            new PotionEffect(MobEffects.INSTANT_DAMAGE),
            new PotionEffect(MobEffects.SLOWNESS, TICKS_PER_SECOND*3, 2))
        );
        cDragon.setParticle(EnumParticleTypes.BLOCK_CRACK, Block.getIdFromBlock(Blocks.DIRT), 0);
        cDragon.setFireballName("Terrain Fireball");
        dragon = cDragon;
        ((net.minecraft.entity.Entity) dragon).setPosition(blockPos.getX(), blockPos.getY(), blockPos.getZ());
        EnderDragon eDragon = (EnderDragon) dragon;
        eDragon.offer(new PDEntityTypeData(getId()));
        eDragon.offer(new OwnerLootData(ownerLoot()));
        eDragon.offer(new PDSpawnData(true));
        eDragon.offer(Keys.DISPLAY_NAME, getDisplayName());
        eDragon.offer(Keys.PERSISTS, true);
        eDragon.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        eDragon.offer(Keys.HEALTH, NEW_HEALTH);
        applyEquipment(eDragon);
        applyLootTable(eDragon);
        return eDragon;
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof EnderDragon)) {
            throw new IllegalArgumentException("Expected a EnderDragon to be given to TerrainDragon to load, but got: " + entity);
        }
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A dragon that gives slowness and uses terrain particles"));
    }

}
