package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SpikeyShulker extends StandardShulkerType {

    private static final double ARMOR_DROP_CHANCE = 0.0;

    public SpikeyShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that has ", TextColors.GRAY, "thorns"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.SILVER;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.GRAY, "Spikey", TextColors.WHITE, " Shulker");
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        List<Enchantment> enchants = new ArrayList<Enchantment>();
        enchants.add(Enchantment.of(EnchantmentTypes.THORNS, 3));
        enchants.add(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1));
        final ItemStack armor = ItemStack.builder()
                .itemType(ItemTypes.CHAINMAIL_HELMET)
                .add(Keys.ITEM_ENCHANTMENTS, enchants)
                .add(Keys.DISPLAY_NAME, Text.of("Shulker shell"))
                .build();
        return super.makeEquipmentLoadout()
                .head(armor, ARMOR_DROP_CHANCE);
    }
}
