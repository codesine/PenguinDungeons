package com.minecraftonline.penguindungeons.customentity.wither;

import java.util.Random;

import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileForImpact;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityWitherSkull;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class SkullProjectile extends EntityWitherSkull {
    private ProjectileForImpact forImpact;
    private ProjectileForImpact forBoostedImpact;
    private boolean boosted = false;

    public SkullProjectile(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ, ProjectileForImpact forImpact) {
        // no boosted projectile variants
        this(world, shooter, accelX, accelY, accelZ, forImpact, forImpact, false);
    }

    public SkullProjectile(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ,
            ProjectileForImpact forImpact, ProjectileForImpact forBoostedImpact) {
        // default 25% chance for projectile to be boosted
        this(world, shooter, accelX, accelY, accelZ, forImpact, forBoostedImpact, 0.25F);
    }

    public SkullProjectile(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ,
            ProjectileForImpact forImpact, ProjectileForImpact forBoostedImpact, float boostedChance) {
        this(world, shooter, accelX, accelY, accelZ, forImpact, forBoostedImpact, new Random().nextFloat() < boostedChance);
    }

    public SkullProjectile(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ,
            ProjectileForImpact forImpact, ProjectileForImpact forBoostedImpact, boolean boosted) {
        super(world, shooter, accelX, accelY, accelZ);
        this.setLocationAndAngles(shooter.posX, shooter.posY + (shooter.height / 2.0F), shooter.posZ, shooter.rotationYaw, shooter.rotationPitch);
        this.forImpact = forImpact;
        this.forBoostedImpact = forBoostedImpact;
        this.boosted = boosted;
        if (this.boosted)
        {
            this.setInvulnerable(true);
        }
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        boolean hit;

        if (!boosted) hit = this.forImpact.forImpact(result, this);
        else   hit = this.forBoostedImpact.forImpact(result, this);

        if (hit) this.setDead();
    }
}
