package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Giant;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;

public class GiantZombie extends ZombieType<Giant> {

    public GiantZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.GIANT;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.DARK_GREEN, "Giant Zombie");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A giant zombie"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        super.onLoad(entity);
        if (!(entity instanceof Giant)) {
            throw new IllegalArgumentException("Expected a Giant to be given to GiantZombie to load, but got: " + entity);
        }
        Giant giant = (Giant) entity;
        EntityCreature creature = (EntityCreature) entity;

        // Giants normally don't have an AI, so add the usual AI for a zombie here
        Goal<Agent> normalGoals = giant.getGoal(GoalTypes.NORMAL).get();
        normalGoals.addTask(0, (AITask<? extends Agent>) new EntityAISwimming(creature));
        normalGoals.addTask(2, (AITask<? extends Agent>) new EntityAIAttackMelee(creature, 1.0D, false));
        normalGoals.addTask(5, (AITask<? extends Agent>) new EntityAIMoveTowardsRestriction(creature, 1.0D));
        normalGoals.addTask(7, (AITask<? extends Agent>) new EntityAIWanderAvoidWater(creature, 1.0D));
        normalGoals.addTask(8, (AITask<? extends Agent>) new EntityAIWatchClosest(creature, EntityPlayer.class, 32.0F)); // giants should be able to see from further away
        normalGoals.addTask(8, (AITask<? extends Agent>) new EntityAILookIdle(creature));
    }
}
