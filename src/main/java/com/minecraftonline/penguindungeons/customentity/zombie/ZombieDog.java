package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.animal.Wolf;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAILeapAtTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;

public class ZombieDog extends ZombieType<Wolf> {

    public ZombieDog(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.WOLF;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.DARK_GREEN, "Zombie Dog");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "An always aggressive zombie wolf"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        super.onLoad(entity);
        if (!(entity instanceof Wolf)) {
            throw new IllegalArgumentException("Expected a Wolf to be given to ZombieDog to load, but got: " + entity);
        }
        Wolf wolf = (Wolf) entity;
        EntityCreature creature = (EntityCreature) wolf;

        // only target players
        Goal<Agent> targetGoals = wolf.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(creature, EntityPlayer.class, true));

        // reduce normal goals (no sitting, following owner, avoiding mobs)
        Goal<Agent> normalGoals = wolf.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();
        normalGoals.addTask(1, (AITask<? extends Agent>) new EntityAISwimming(creature));
        normalGoals.addTask(4, (AITask<? extends Agent>) new EntityAILeapAtTarget(creature, 0.4F));
        normalGoals.addTask(5, (AITask<? extends Agent>) new EntityAIAttackMelee(creature, 1.0D, true));
        normalGoals.addTask(8, (AITask<? extends Agent>) new EntityAIWanderAvoidWater(creature, 1.0D));
        normalGoals.addTask(10, (AITask<? extends Agent>) new EntityAIWatchClosest(creature, EntityPlayer.class, 8.0F));
        normalGoals.addTask(10, (AITask<? extends Agent>) new EntityAILookIdle(creature));
    }

}
