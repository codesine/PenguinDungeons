/*
 * CraftBook Copyright (C) 2010-2021 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2021 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.penguindungeons.data.customentity;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class PDEntityTypeDataBuilder extends AbstractDataBuilder<PDEntityTypeData> implements DataManipulatorBuilder<PDEntityTypeData, ImmutablePDEntityTypeData> {
	public PDEntityTypeDataBuilder() {
		super(PDEntityTypeData.class, 1);
	}

	@Override
	public PDEntityTypeData create() {
		return new PDEntityTypeData(ResourceKey.of("dummy", "dummy"));
	}

	@Override
	public Optional<PDEntityTypeData> createFrom(DataHolder dataHolder) {
		return create().fill(dataHolder);
	}

	@Override
	protected Optional<PDEntityTypeData> buildContent(DataView container) throws InvalidDataException {
		if (container.contains(PenguinDungeonKeys.PD_ENTITY_TYPE.getQuery())) {
			String str = container.getString(PenguinDungeonKeys.PD_ENTITY_TYPE.getQuery()).get();
			final ResourceKey key;
			try {
				key = ResourceKey.resolve(str);
			} catch (IllegalArgumentException e) {
				throw new InvalidDataException("Invalid ResourceKey", e);
			}
			return Optional.of(new PDEntityTypeData(key));
		}
		return Optional.empty();
	}
}
