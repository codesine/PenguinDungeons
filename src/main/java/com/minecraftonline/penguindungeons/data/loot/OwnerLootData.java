package com.minecraftonline.penguindungeons.data.loot;

import java.util.Optional;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractBooleanData;
import org.spongepowered.api.data.merge.MergeFunction;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class OwnerLootData extends AbstractBooleanData<OwnerLootData, ImmutableOwnerLootData>
{

	public OwnerLootData(boolean value)
	{
        super(PenguinDungeonKeys.OWNER_LOOT, value, false);
    }

    @Override
    public Optional<OwnerLootData> fill(DataHolder dataHolder, MergeFunction overlap)
    {
        dataHolder.get(OwnerLootData.class).ifPresent((data) -> {
            OwnerLootData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<OwnerLootData> from(DataContainer container)
    {
        Optional<Boolean> optBoolean = container.getBoolean(PenguinDungeonKeys.OWNER_LOOT.getQuery());
        return optBoolean.map(OwnerLootData::new);
    }

    @Override
    public OwnerLootData copy()
    {
        return new OwnerLootData(this.getValue());
    }

    @Override
    public ImmutableOwnerLootData asImmutable()
    {
        return new ImmutableOwnerLootData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }
}
