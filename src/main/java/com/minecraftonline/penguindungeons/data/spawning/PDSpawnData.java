package com.minecraftonline.penguindungeons.data.spawning;

import java.util.Optional;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractBooleanData;
import org.spongepowered.api.data.merge.MergeFunction;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class PDSpawnData extends AbstractBooleanData<PDSpawnData, ImmutablePDSpawnData>
{

	public PDSpawnData(boolean value)
	{
        super(PenguinDungeonKeys.PD_SPAWN, value, false);
    }

    @Override
    public Optional<PDSpawnData> fill(DataHolder dataHolder, MergeFunction overlap)
    {
        dataHolder.get(PDSpawnData.class).ifPresent((data) -> {
            PDSpawnData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<PDSpawnData> from(DataContainer container)
    {
        Optional<Boolean> optBoolean = container.getBoolean(PenguinDungeonKeys.PD_SPAWN.getQuery());
        return optBoolean.map(PDSpawnData::new);
    }

    @Override
    public PDSpawnData copy()
    {
        return new PDSpawnData(this.getValue());
    }

    @Override
    public ImmutablePDSpawnData asImmutable()
    {
        return new ImmutablePDSpawnData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }
}
