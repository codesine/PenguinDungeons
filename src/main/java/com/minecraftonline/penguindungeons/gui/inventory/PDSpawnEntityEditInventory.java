package com.minecraftonline.penguindungeons.gui.inventory;

import com.google.common.collect.Sets;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.config.PDConfigEntityRegistry;
import com.minecraftonline.penguindungeons.customentity.FireworkHelper;
import com.minecraftonline.penguindungeons.customentity.NBTEntityType;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.customentity.SimpleEntityType;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.gui.sign.SignGui;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import com.minecraftonline.penguindungeons.util.weightedtable.WeightedTableReference;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.event.item.inventory.InteractInventoryEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.InventoryArchetypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.item.inventory.property.InventoryTitle;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.inventory.property.SlotPos;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.inventory.transaction.SlotTransaction;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;
import org.spongepowered.common.util.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class PDSpawnEntityEditInventory {

    public enum SpawnInventoryType {
        DUNGEON_NODE,
        MOB_SPAWNER
    }

    private static final ResourceKey dummyKey = ResourceKey.pd("dummy");

    private static final double DEFAULT_WEIGHT = 1;

    private static final Text MISSING_PERMISSION = Text.of(TextColors.RED, "You don't have permission to use this entity!");
    private static final DecimalFormat PROBABILITY_DECIMAL_FORMAT = new DecimalFormat("#.##");

    private static final int WIDTH = 9;
    private static final int HEIGHT = 3;

    private final WeightedTableReference<PDEntityType> weightedTableReference;
    private final Set<Player> players = new HashSet<>(); // Players who have this inventory open.
    private final List<PDEntityType> entities = new ArrayList<>();
    private final Inventory inventory;
    private final SpawnInventoryType type;


    public void showInventoryTo(Player player) {
        player.openInventory(this.inventory);
        this.players.add(player);
    }

    public void onDisconnect(Player player) {
        this.players.remove(player);
    }

    protected boolean supportsDelete() {
        return false;
    }

    protected void delete() {}

    public PDSpawnEntityEditInventory(WeightedTableReference<PDEntityType> weightedTableReference, Text title, SpawnInventoryType type) {
        this.weightedTableReference = weightedTableReference;
        this.type = type;
        this.inventory = Inventory.builder()
                .of(InventoryArchetypes.MENU_GRID)
                .property(InventoryTitle.of(title))
                .property(InventoryDimension.of(WIDTH, HEIGHT))
                .listener(InteractInventoryEvent.Close.class, event -> event.getCause().first(Player.class).ifPresent(this.players::remove))
                .listener(ClickInventoryEvent.class, event -> {
                    try {
                        event.setCancelled(true);
                        if (!event.getSlot().isPresent()) {
                            return;
                        }
                        Optional<Player> optPlayer = event.getCause().first(Player.class);
                        if (!optPlayer.isPresent()) {
                            return;
                        }
                        Player player = optPlayer.get();
                        if (event instanceof ClickInventoryEvent.Shift) {
                            for (SlotTransaction transaction : event.getTransactions()) {
                                if (!transaction.getOriginal().isEmpty()) {
                                    // The sauce.

                                    Optional<PDEntityType> pdEntityType = fromSpawnEgg(transaction.getOriginal());
                                    if (!pdEntityType.isPresent()) {
                                        tryAddNewSpawnEgg(event, transaction, transaction.getOriginal(), player);
                                        continue;
                                    }
                                    if (this.weightedTableReference.has(pdEntityType.get())) {
                                        return;
                                    }
                                    if (!player.hasPermission(pdEntityType.get().getPermission())) {
                                        player.sendMessage(MISSING_PERMISSION);
                                        return;
                                    }
                                    try {
                                        this.weightedTableReference.put(pdEntityType.get(), DEFAULT_WEIGHT);
                                    } catch (WeightedTableReference.KeyNotValidException e) {
                                        sendUnsupportedEntityTypeMessage(player);
                                        return;
                                    }
                                    updateInventory();
                                    event.setCancelled(false); // Because cancelling it bugs it out yay woohoo.
                                    transaction.setValid(false); // This doesn't break it, hmmmmm thonk.
                                }
                            }
                        }
                        else if (event instanceof ClickInventoryEvent.Primary) {
                            for (SlotTransaction slotTransaction : event.getTransactions()) {
                                int slotIndex = slotTransaction.getSlot().getInventoryProperty(SlotIndex.class).map(SlotIndex::getValue).get();
                                if (slotIndex >= WIDTH * HEIGHT) {
                                    event.setCancelled(false); // To do with the users inventory.
                                    return; // Not to do with the spawn eggs on the top row.
                                }
                                if (slotTransaction.getOriginal().isEmpty()) {
                                    if (slotIndex != this.weightedTableReference.entries().size()) {
                                        return;
                                    }
                                    Optional<PDEntityType> pdEntityType = fromSpawnEgg(slotTransaction.getFinal());
                                    if (!pdEntityType.isPresent()) {
                                        tryAddNewSpawnEgg(event, slotTransaction, slotTransaction.getFinal(), player);
                                        continue;
                                    }
                                    if (this.weightedTableReference.has(pdEntityType.get())) {
                                        return;
                                    }
                                    try {
                                        this.weightedTableReference.put(pdEntityType.get(), DEFAULT_WEIGHT);
                                    } catch (WeightedTableReference.KeyNotValidException e) {
                                        sendUnsupportedEntityTypeMessage(player);
                                        return;
                                    }
                                }
                                else {
                                    if (slotIndex == (WIDTH * HEIGHT - 1) && supportsDelete()) {
                                        // Delete this node.
                                        delete();
                                        return;
                                    }
                                    if (slotIndex >= WIDTH) {
                                        if (slotIndex < WIDTH * 2) {
                                            // To do with weights.
                                            final int aboveSlot = slotIndex - WIDTH;
                                            final Optional<PDEntityType> pdEntityType = getFromIndex(aboveSlot);
                                            if (!pdEntityType.isPresent()) {
                                                return;
                                            }
                                            this.clickWeight(player, pdEntityType.get());
                                        }
                                        return; // Not to do with the spawn eggs on the top row.
                                    }
                                    final Optional<PDEntityType> pdEntityType = getFromIndex(slotIndex);
                                    if (!pdEntityType.isPresent()) {
                                        continue;
                                    }
                                    if (!this.weightedTableReference.has(pdEntityType.get())) {
                                        return;
                                    }
                                    if (!player.hasPermission(pdEntityType.get().getPermission())) {
                                        player.sendMessage(MISSING_PERMISSION);
                                        return;
                                    }
                                    this.weightedTableReference.remove(pdEntityType.get());
                                }
                                event.getCursorTransaction().setValid(false);
                                event.setCancelled(false);
                                updateInventory();
                            }
                        }
                    } catch (WeightedTableReference.ReferenceNoLongerValid e) {
                        this.closeAllOpenInventories();
                    }
                })
                .build(PenguinDungeons.getInstance());
        try {
            updateInventory();
        } catch (WeightedTableReference.ReferenceNoLongerValid e) {
            throw new IllegalArgumentException("We started with an invalid reference!", e);
        }
    }

    private Optional<PDEntityType> getFromIndex(int index) {
        if (this.entities.size() > index) {
            return Optional.of(this.entities.get(index));
        }
        return Optional.empty();
    }

    private void updateInventory() throws WeightedTableReference.ReferenceNoLongerValid {
        this.entities.clear();
        this.inventory.clear();
        int totalWeight = (int) this.weightedTableReference.totalWeight();
        int i = 0;
        for (Map.Entry<PDEntityType, Double> entry : weightedTableReference.entries()) {
            this.entities.add(entry.getKey());
            this.inventory.query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(i, 0))).set(entry.getKey().getSpawnEgg());

            final double percent = (entry.getValue() * 100.0) / totalWeight;
            final String percentString = PROBABILITY_DECIMAL_FORMAT.format(percent) + "%";

            final ItemStack itemStack = ItemStack.builder()
                    .itemType(ItemTypes.GOLD_NUGGET)
                    .add(Keys.DISPLAY_NAME, Text.of(TextColors.GRAY, "Weight - ", TextColors.GOLD, entry.getValue()))
                    .add(Keys.ITEM_LORE, Collections.singletonList(Text.of(TextColors.GRAY, "Chance: ", TextColors.GOLD, percentString)))
                    .build();
            this.inventory.query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(i, 1))).set(itemStack);
            i++;
        }

        if (supportsDelete()) {
            ItemStack deleteItem = ItemStack.builder()
                    .itemType(ItemTypes.BARRIER)
                    .add(Keys.DISPLAY_NAME, Text.of(TextColors.RED, "Delete node"))
                    .add(Keys.ITEM_LORE, Collections.singletonList(Text.of(TextColors.GRAY, "Click to delete this node")))
                    .build();
            inventory.query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(WIDTH - 1, HEIGHT - 1))).set(deleteItem);
        }
    }

    private static final Set<DataQuery> SIMPLE_SPAWN_EGG_KEYS = Sets.newHashSet(DataQuery.of("EntityTag"), DataQuery.of("EntityTag", "id"));

    private static Optional<PDEntityType> fromSpawnEgg(ItemStackSnapshot snapshot) {
        Optional<PDEntityType> pdEntityTypeOptional = snapshot.get(PenguinDungeonKeys.PD_ENTITY_TYPE).flatMap(PenguinDungeons.getInstance().getPDEntityRegistry()::find);
        if (pdEntityTypeOptional.isPresent()) {
            return pdEntityTypeOptional;
        }

        if (!snapshot.get(Keys.SPAWNABLE_ENTITY_TYPE).isPresent()) {
            return Optional.empty();
        }

        DataView nbtView = snapshot.toContainer().getView(Constants.Sponge.UNSAFE_NBT)
                .orElseThrow(() -> new IllegalStateException("Spawn egg contains no nbt!"));
        Set<DataQuery> keys = nbtView.getKeys(true);

        if (keys.equals(SIMPLE_SPAWN_EGG_KEYS)) {
            return Optional.of(new SimpleEntityType(snapshot.require(Keys.SPAWNABLE_ENTITY_TYPE)));
        }

        Optional<PDEntityType> simpleEntityType = snapshot.get(Keys.SPAWNABLE_ENTITY_TYPE)
                .map(SimpleEntityType::new);
        if (!simpleEntityType.isPresent()) {
            return simpleEntityType;
        }

        return Optional.empty();
    }

    private static Optional<PDEntityType> fromSpawnEggOrCustom(ItemStackSnapshot snapshot) {
        Optional<PDEntityType> entityType = fromSpawnEgg(snapshot);
        if (entityType.isPresent()) {
            return entityType;
        }
        return PDConfigEntityRegistry.getKey(snapshot).flatMap(PenguinDungeons.getInstance().getPDEntityRegistry()::find);
    }

    private void tryAddNewSpawnEgg(ClickInventoryEvent event, SlotTransaction transaction, ItemStackSnapshot snapshot, Player player) throws WeightedTableReference.ReferenceNoLongerValid {
        // currently only allow inserting fireworks in mob spawners
        // because Sponge errors when trying to convert fireworks to JSON
        // causing the nbt entity config saving to fail on server stop
        // which then causes the dungeon config loading to fail on server start
        // because it includes an entity that is not in the entity config
        if (snapshot.getType() == ItemTypes.FIREWORKS && type == SpawnInventoryType.MOB_SPAWNER) {
            // fireworks item, replace item with spawn egg
            snapshot = FireworkHelper.toSpawnEgg(snapshot);
        }

        if (!snapshot.get(Keys.SPAWNABLE_ENTITY_TYPE).isPresent()) {
            return;
        }

        DataView nbtView = snapshot.toContainer().getView(Constants.Sponge.UNSAFE_NBT)
                .orElseThrow(() -> new IllegalStateException("Spawn egg or firework contains no nbt!"));

        Set<DataQuery> keys = nbtView.getKeys(true);

        if (keys.equals(SIMPLE_SPAWN_EGG_KEYS)) {
            return;
        }

        ResourceKey key;

        // Mob Spawners do not need to save these or require the item to be named
        if (this.type != SpawnInventoryType.MOB_SPAWNER) {
            // Super cool NBT custom mob!
            key = PDConfigEntityRegistry.getKey(snapshot)
                    .orElseThrow(() -> new IllegalArgumentException("Custom Mobs Eggs must be named!"));

            Optional<NBTEntityType> optNbtEntityType = PenguinDungeons.getInstance().getPDConfigEntityRegistry().find(key);

            if (optNbtEntityType.isPresent()) {
                // Note: .equals() checks the path, so to be sure we're doing a proper
                // comparison, we copy both, to make them both have the same path.
                DataContainer nbtContainer = nbtView.copy();
                if (optNbtEntityType.get().getNbt().copy().equals(nbtContainer)) {
                    // An existing, and a match.
                    if (!weightedTableReference.has(optNbtEntityType.get())) {
                        try {
                            weightedTableReference.put(optNbtEntityType.get(), DEFAULT_WEIGHT);
                        } catch (WeightedTableReference.KeyNotValidException e) {
                            player.sendMessage(Text.of(TextColors.RED));
                        }
                        event.setCancelled(false);
                        transaction.setValid(false);
                        updateInventory();
                    }
                    return;
                }

                // It already exists, but it differs, offer up and option the user.
                ItemStack confirmButton = ItemStack.builder()
                        .itemType(ItemTypes.BARRIER)
                        .add(Keys.DISPLAY_NAME, Text.of(TextColors.RED, TextStyles.BOLD, "Confirm changing this custom entity"))
                        .add(Keys.ITEM_LORE, Arrays.asList(
                                Text.of(TextColors.BLUE, "Entity to change: '", TextColors.RED, key.asString(), TextColors.BLUE, "'"),
                                Text.of(TextColors.WHITE, "This custom entity has the same name as"),
                                Text.of(TextColors.WHITE, "one already entered but different NBT!"),
                                Text.of(TextColors.GOLD, TextStyles.BOLD, "If you proceed:"),
                                Text.of(TextColors.WHITE, "You will overwrite the existing NBT"),
                                Text.of(TextColors.WHITE, "and change all uses of this entity to"),
                                Text.of(TextColors.WHITE, "to use this new NBT."),
                                Text.of(TextColors.GOLD, TextStyles.BOLD, "Alternatively:"),
                                Text.of(TextColors.WHITE, "You may rename this entity, and then"),
                                Text.of(TextColors.WHITE, "you will create a new custom entity,"),
                                Text.of(TextColors.WHITE, "leaving the existing ones intact.")
                                ))
                        .build();

                ConfirmationPopup confirmationPopup = new ConfirmationPopup(confirmButton, () -> {
                    optNbtEntityType.get().setNbt(nbtView.copy());
                    try {
                        if (!weightedTableReference.has(optNbtEntityType.get())) {
                            weightedTableReference.put(optNbtEntityType.get(), DEFAULT_WEIGHT);
                            updateInventory();
                        }
                    } catch (WeightedTableReference.ReferenceNoLongerValid e) {
                        player.sendMessage(Text.of(TextColors.RED, "The thing you were editing no longer exists."));
                        closeAllOpenInventories();
                    } catch (WeightedTableReference.KeyNotValidException e) {
                        sendUnsupportedEntityTypeMessage(player);
                    }
                }, Text.of(TextColors.RED, "Confirm changing custom entity"), this.inventory);

                Task.builder()
                        .delayTicks(1)
                        .execute(() -> player.openInventory(confirmationPopup.create()))
                        .submit(PenguinDungeons.getInstance());
                return;
            }
        } else {
            // dummy key if none given
            key = PDConfigEntityRegistry.getKey(snapshot).orElse(dummyKey);
        }

        // Brand new egg
        final NBTEntityType nbtEntityType = new NBTEntityType(key, snapshot.require(Keys.SPAWNABLE_ENTITY_TYPE), nbtView.copy());
        if (this.type != SpawnInventoryType.MOB_SPAWNER) {
            // does not apply to mob spawners
            PenguinDungeons.getInstance().getPDConfigEntityRegistry().register(nbtEntityType.getId(), nbtEntityType);
        }
        try {
            weightedTableReference.put(nbtEntityType, DEFAULT_WEIGHT);
        } catch (WeightedTableReference.KeyNotValidException e) {
            sendUnsupportedEntityTypeMessage(player);
            return;
        }
        updateInventory();
        event.setCancelled(false);
        transaction.setValid(false);
    }

    private void clickWeight(Player player, PDEntityType pdEntityType) throws WeightedTableReference.ReferenceNoLongerValid {
        Double currentWeight = this.weightedTableReference.getWeight(pdEntityType);
        List<Text> lines = Arrays.asList(
                Text.of(currentWeight == null ? "" : currentWeight.intValue()),
                Text.of("^^^^^^^^^^^^^^^^^"),
                Text.of("Enter the new"),
                Text.of("weight above")
        );
        final SignGui gui = new SignGui(lines, (ply, newLines) -> {
            try {
                final int newWeight = Integer.parseInt(newLines[0]);
                if (newWeight < 1) {
                    ply.sendMessage(Text.of(TextColors.RED, "You must set a weight of atleast 1!"));
                    this.players.remove(player);
                    return;
                }
                this.weightedTableReference.put(pdEntityType, newWeight);
                this.updateInventory();
                ply.openInventory(this.inventory); // And.. open it back up again.
            } catch (NumberFormatException e) {
                ply.sendMessage(Text.of(TextColors.RED, "You must input a valid number!"));
                this.players.remove(player);
            } catch (WeightedTableReference.ReferenceNoLongerValid e) {
                ply.sendMessage(Text.of(TextColors.RED, "The item you were editing no longer exists."));
                closeAllOpenInventories();
            } catch (WeightedTableReference.KeyNotValidException e) {
                throw new IllegalStateException("A weight of a previously valid item was edited, but could not be changed as the key was no longer valid..");
            }
        });

        Task.builder()
                .delayTicks(1)
                .execute(() -> {
                    player.closeInventory();
                    PenguinDungeons.getInstance().getOpenSignEditorManager().openSignGui(player, gui);
                })
                .submit(PenguinDungeons.getInstance());
    }

    protected void closeAllOpenInventories() {
        this.players.forEach(Player::closeInventory);
    }

    private void sendUnsupportedEntityTypeMessage(Player player) {
        player.sendMessage(Text.of(TextColors.RED, "This entity is not supported here."));
    }

}
