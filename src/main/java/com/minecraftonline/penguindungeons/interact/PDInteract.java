package com.minecraftonline.penguindungeons.interact;

import java.util.Optional;

import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.animal.Wolf;
import org.spongepowered.api.entity.living.monster.Husk;
import org.spongepowered.api.entity.living.monster.ZombieVillager;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class PDInteract {
    @Listener
    public void onInteractPDEntity(InteractEntityEvent event, @First Player player)
    {
        Optional<ItemStackSnapshot> usedItemOptional = event.getContext().get(EventContextKeys.USED_ITEM);
        if (!usedItemOptional.isPresent()) return;

        ItemStackSnapshot usedItem = usedItemOptional.get();
        Entity target = event.getTargetEntity();

        Optional<ResourceKey> pdType = target.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (pdType.isPresent())
        {
            if (usedItem.getType() == ItemTypes.NAME_TAG || usedItem.getType() == ItemTypes.LEAD)
            {
                // deny name tag use and lead
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.GOLDEN_APPLE && target instanceof ZombieVillager)
            {
                // deny zombie villager curing
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.BONE && target instanceof Wolf)
            {
                // deny wolf taming
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.NETHER_STAR && target instanceof Husk)
            {
                if (pdType.get().equals(CustomEntityTypes.PUMPKIN_ZOMBIE.getId()))
                {
                    Optional<ItemStack> hat = ((Husk)target).getHelmet();
                    if (hat.isPresent() && hat.get().getType() == ItemTypes.LIT_PUMPKIN)
                    {
                        // don't create multiple bosses from the same entity
                        if (target.isRemoved()) return;

                        // this is a boosted version, convert to boss mob upon using a nether star

                        if (player.gameMode().get() != GameModes.CREATIVE)
                        {
                            // use up the item
                            ItemStack item = usedItem.createStack();
                            item.setQuantity(item.getQuantity() - 1);
                            HandType hand = HandTypes.MAIN_HAND;
                            Optional<ItemStack> handItem = player.getItemInHand(hand);
                            if (handItem.isPresent() && handItem.get().getType() != ItemTypes.NETHER_STAR) hand = HandTypes.OFF_HAND;
                            player.setItemInHand(hand, item);
                        }

                        // replace with boss
                        Vector3d position = target.getLocation().getPosition();
                        player.playSound(SoundTypes.ENTITY_ELDER_GUARDIAN_CURSE, position, 1);
                        CustomEntityTypes.PUMPKIN_WITHER.createEntity(target.getWorld(), position).spawn(target.getWorld());
                        target.remove();
                    }
                }
            }
        }
    }
}
