package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityGolem;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.entity.living.golem.IronGolem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityIronGolem.class)
public abstract class EntityIronGolemMixin extends EntityGolem {

    private boolean isFrostGolem()
    {
        Optional<ResourceKey> id = ((IronGolem) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        return id.isPresent() && id.get().equals(CustomEntityTypes.FROST_GOLEM.getId());
    }

    public EntityIronGolemMixin(World worldIn) {
        super(worldIn);
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        if (this.getHealth() < (this.getMaxHealth() / 2))
        {
            // Frost Golems don't take damage from most projectiles or explosions when under half health
            if (this.isFrostGolem())
            {
                if (source.isExplosion()) return false;
                Entity entity = source.getImmediateSource();
                if (entity instanceof EntityArrow || entity instanceof EntityShulkerBullet || entity instanceof EntityFireball) {
                    return false;
                }
            }
        }

        boolean result = super.attackEntityFrom(source, amount);

        return result;
    }

    @Inject(method = "collideWithEntity", at = @At("HEAD"), cancellable = true)
    private void onCollideWithEntity(Entity entityIn, CallbackInfo ci) {
        IronGolem golem = (IronGolem) this;
        final Optional<ResourceKey> id = golem.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (id.isPresent())
        {
            // don't attack regular hostile mobs that bump in to custom iron golem
            ci.cancel();
        }
     }
}
