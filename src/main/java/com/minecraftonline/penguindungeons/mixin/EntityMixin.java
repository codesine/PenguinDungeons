package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.BossInfo;
import net.minecraft.world.BossInfoServer;
import net.minecraft.world.World;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.minecraftonline.penguindungeons.customentity.golem.HasBossBar;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarData;

@Mixin(EntityLiving.class)
public abstract class EntityMixin extends EntityLivingBase implements HasBossBar {

    private BossInfoServer bossInfo;
    private boolean usesBossBar = false;

    public EntityMixin(World worldIn) {
        super(worldIn);
    }

    @Override
    public void useBossBar(boolean showBossBar)
    {
        ((Entity) this).offer(new PDBossBarData(showBossBar));
        this.usesBossBar = showBossBar;
        if (this.bossInfo != null) this.bossInfo.setVisible(showBossBar);
    }

    private BossInfoServer checkBossInfo()
    {
        // this has to be done here instead of in constructor because mixins constructors are weird
        if (this.bossInfo == null)
        {
            this.usesBossBar = ((Entity) this).get(PenguinDungeonKeys.PD_BOSS_BAR).orElse(false);
            if (this.usesBossBar)
            {
                this.bossInfo = new BossInfoServer(this.getDisplayName(), BossInfo.Color.BLUE, BossInfo.Overlay.PROGRESS);
                this.bossInfo.setDarkenSky(false);
                this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
            }
        }
        return this.bossInfo;
    }

    @Inject(method = "readEntityFromNBT", at = @At("HEAD"))
    private void onReadEntityFromNBT(NBTTagCompound compound, CallbackInfo ci) {
        this.checkBossInfo();
        if (this.usesBossBar && this.hasCustomName())
        {
            this.bossInfo.setName(this.getDisplayName());
        }
    }

    @Override
    public void addTrackingPlayer(EntityPlayerMP player) {
        this.checkBossInfo();
        if (this.usesBossBar) this.bossInfo.addPlayer(player);
        super.addTrackingPlayer(player);
    }

    @Override
    public void removeTrackingPlayer(EntityPlayerMP player) {
        this.checkBossInfo();
        if (this.usesBossBar) this.bossInfo.removePlayer(player);
        super.removeTrackingPlayer(player);
    }

    @Inject(method = "onLivingUpdate", at = @At("HEAD"))
    public void onOnLivingUpdate(CallbackInfo ci) {
        this.checkBossInfo();
        if (this.usesBossBar) this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
    }
}
