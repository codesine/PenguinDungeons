package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.entity.living.Hostile;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityMob.class)
public abstract class EntityMobMixin extends EntityLiving {

    public EntityMobMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "getCanSpawnHere", at = @At("HEAD"), cancellable = true)
    private void onGetCanSpawnHere(CallbackInfoReturnable<Boolean> cir) {
        Hostile mob = (Hostile) this; // hostile == mob
        final Optional<ResourceKey> id = mob.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (id.isPresent())
        {
            // bypass lighting spawn restriction
            IBlockState iblockstate = this.world.getBlockState((new BlockPos(this)).down());
            cir.setReturnValue(this.world.getDifficulty() != EnumDifficulty.PEACEFUL && iblockstate.canEntitySpawn(this));
        }
    }
}
