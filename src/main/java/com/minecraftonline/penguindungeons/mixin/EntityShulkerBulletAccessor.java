package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(EntityShulkerBullet.class)
public interface EntityShulkerBulletAccessor {

    @Accessor("owner")
    public EntityLivingBase getOwner();
}
