package com.minecraftonline.penguindungeons.mixin;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(targets = "net.minecraft.world.storage.loot.LootTableManager$Loader")
public class LootTableLoadMixin {

    @Inject(method = "Lnet/minecraft/world/storage/loot/LootTableManager$Loader;loadBuiltinLootTable(Lnet/minecraft/util/ResourceLocation;)Lnet/minecraft/world/storage/loot/LootTable;",
            at = @At("HEAD"),
            cancellable = true)
    private void onLoad(ResourceLocation resource, CallbackInfoReturnable<LootTable> cir) {
        if (resource.getNamespace().equals(ResourceKey.PENGUIN_DUNGEONS_NAMESPACE)) {
            LootTable lootTable = PenguinDungeons.getInstance().getLootTableManager().loadLootTable(resource);
            cir.setReturnValue(lootTable);
        }
    }
}
