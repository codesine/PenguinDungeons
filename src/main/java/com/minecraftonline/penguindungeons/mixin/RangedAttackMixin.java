package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIAttackRanged;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EntityAIAttackRanged.class)
public abstract class RangedAttackMixin {

    @Shadow
    private EntityLiving entityHost;

    @Inject(method = "shouldExecute", at = @At("HEAD"), cancellable = true)
    private void onShouldExecute(CallbackInfoReturnable<Boolean> cir) {
        EntityLivingBase entitylivingbase = this.entityHost.getAttackTarget();
        // fix ranged mobs continuing to fire at targets which are already dead
        if (entitylivingbase != null && !entitylivingbase.isEntityAlive()) cir.setReturnValue(false);
    }
}
