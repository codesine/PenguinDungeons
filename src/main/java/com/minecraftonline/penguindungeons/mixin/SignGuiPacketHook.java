package com.minecraftonline.penguindungeons.mixin;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.gui.sign.SignGui;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.play.client.CPacketUpdateSign;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(NetHandlerPlayServer.class)
public abstract class SignGuiPacketHook {

    @Shadow public EntityPlayerMP player;

    @Inject(method = "processUpdateSign", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/player/EntityPlayerMP;markPlayerActive()V"))
    public void hookSignUpdatePacket(CPacketUpdateSign packetIn, CallbackInfo ci) {
        Player player = (Player) this.player;
        final SignGui signGui = PenguinDungeons.getInstance().getOpenSignEditorManager().consumeOpenSign(player);
        if (signGui == null) {
            return;
        }
        ci.cancel();
        signGui.getHook().accept(player, packetIn.getLines());
    }
}
