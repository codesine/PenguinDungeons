package com.minecraftonline.penguindungeons.util;

import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.HandTypes;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class HeldItemMap<K, V> {
    private final Map<K, HeldItems<V>> map = new HashMap<>();

    public V put(K key, V value, HandType handType) {
        final HeldItems<V> old = map.get(key);
        if (old != null) {
            return apply(old, value, handType);
        }
        final HeldItems<V> heldItems = new HeldItems<V>();
        apply(heldItems, value, handType);
        return null;
    }

    public V remove(K key, HandType handType) {
        HeldItems<V> heldItems = map.get(key);
        if (heldItems == null) {
            return null;
        }
        return this.remove(heldItems, handType);
    }

    private V apply(HeldItems<V> heldItems, V value, HandType handType) {
        if (handType == HandTypes.MAIN_HAND) {
            V old = heldItems.mainHand;
            heldItems.mainHand = value;
            return old;
        }
        else if (handType == HandTypes.OFF_HAND) {
            V old = heldItems.offHand;
            heldItems.offHand = value;
            return old;
        }
        else {
            throw new IllegalStateException("Unknown HandType: " + handType);
        }
    }

    private V remove(HeldItems<V> heldItems, HandType handType) {
        if (handType == HandTypes.MAIN_HAND) {
            V old = heldItems.mainHand;
            heldItems.mainHand = null;
            return old;
        }
        else if (handType == HandTypes.OFF_HAND) {
            V old = heldItems.offHand;
            heldItems.offHand = null;
            return old;
        }
        else {
            throw new IllegalStateException("Unknown HandType: " + handType);
        }
    }

    public boolean containsEntry(final K key, V value) {
        final HeldItems<V> heldItems = map.get(key);
        return heldItems != null && value.equals(heldItems.mainHand) && value.equals(heldItems.offHand);
    }

    public void clear() {
        this.map.clear();
    }

    public static class HeldItems<V> {
        @Nullable
        public V mainHand;
        @Nullable
        public V offHand;
    }
}
